# Translation Proxy #

A reverse proxy that intercepts the response and translates the content of the page before returning it to the user. 

### How do I get set up? ###

* You don't. This code relies on a companion app for shared database connection, cache, data models, background tasks and utility functions. Email bjfinnerty@gmail.com for more info.
