__author__ = 'brianfinnerty'
import urllib2, os, re

from datetime import datetime, timedelta
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from urlparse import urljoin, urlparse, urlunparse
from bs4 import Comment



def set_expire_time(session):

    now_string = session.headers.get("date")
    now_datetime = datetime.strptime(now_string, "%a, %d %b %Y %H:%M:%S GMT")
    #now_datetime= datetime.fromtimestamp(mktime(now_time))
    couch_session_expiry = now_datetime + timedelta(minutes=60)
    string_time = datetime.strftime(couch_session_expiry, "%a, %d %b %Y %H:%M:%S GMT")
    return string_time




def make_links_absolute(soup, url):

    if soup.base:
        url = soup.base.get('href')

    parsed_url = urlparse(url)
    new_url = urlunparse(['', parsed_url.netloc, parsed_url.path, parsed_url.params, parsed_url.query, parsed_url.fragment])
    anchors = soup.findAll("a", href=True)
    scripts = soup.findAll("script", src=True)
    styles = soup.findAll("link", {"href":True,"rel":"stylesheet"})
    images = soup.findAll("img", src=True)
    flash_embeds = soup.findAll('embed', src=True)
    flash_objects = soup.findAll("param", {'name':'movie', 'value':True})
    forms = soup.findAll('form', action=True)
    inline_styles = soup.findAll('style', href=False)

    links = anchors + scripts + styles + images + flash_embeds + flash_objects + forms

    for link in links:

        if link.get("src"):
            link["src"] = urljoin(new_url,link["src"])
        elif link.get("href"):
            link["href"] = urljoin(new_url,link["href"])
        elif link.get("value"):
            link["value"] = urljoin(new_url,link["value"])
        elif link.get("action"):
            link["action"] = urljoin(new_url, link["action"])

    # for style in inline_styles:
    #     if style.text.find("@import") > -1:
    #         sheet = cssutils.parseString(style.text)
    #         import_rules = [i for i in sheet.cssRules if i.type == 3]
    #         for r in import_rules:
    #             new_rule = cssutils.css.CSSImportRule(href=urljoin(url, r.href))
    #             sheet.insertRule(new_rule)
    #             sheet.deleteRule(r)
    #         style.string.replace_with(sheet.cssText)

    return soup



def fake_links_absolute(soup, url):

    # get all links. if they are of the same domain as the root, strip back to root link
    anchors = soup.findAll("a", href=True)
    area_links = soup.findAll("area",href=True) # yes some clowns actually use this shit
    for link in anchors+area_links:
        if is_domain(link["href"], url) and "mailto" not in link["href"]:
            link["href"] = urljoin("http://hosting.reverbeo.com/", "<?php echo $lang ?>/" + link["href"].lstrip("http://"))
    return soup




def is_domain(url, root):
    url_domain = urlparse(url.replace("www.","")).netloc

    if not url_domain:
        # relative link
        return True
    else:
        root_domain = urlparse(root.replace("www.","")).netloc
        if root_domain == url_domain:
            return True
        else:
            return False



def uniquify_list(seq):
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if x not in seen and not seen_add(x)]



def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False





def parse_cookie_string(qstring):
    cookie_dict = {}
    qstring_list = qstring.split(";")
    for i in qstring_list:
        cookie_dict[urllib2.unquote(i.split('=')[0]).strip()] = qs_key = i.split('=')[1]
    return cookie_dict


def findnth(haystack, needle, n):
    parts= haystack.split(needle, n+1)
    if len(parts)<=n+1:
        return -1
    return len(haystack)-len(parts[-1])-len(needle)


def is_demo(req_uri):
    req_domain = urlparse(req_uri).netloc
    pattern1 = r'(\w{2}-\w+\.reverbeo\.[com|dev])'

    if re.match(pattern1, req_domain):
        return True
    else:
        return False



# requests monkey patch
# http://stackoverflow.com/questions/14390605/python-requests-ssl-issue/21150722#21150722
import requests

def fileno(self):
    return self.socket.fileno()


def close(self):
    return self.connection.shutdown()


requests.pyopenssl.WrappedSocket.close = close
requests.pyopenssl.WrappedSocket.fileno = fileno

