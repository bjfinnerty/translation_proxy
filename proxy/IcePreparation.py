__author__ = 'brianfinnerty'

from bs4 import BeautifulSoup, Tag as bs4tag, Comment as bs4comment, NavigableString as ns4
from urlparse import urlparse
from ProjectDetails import ProjectDetails
from PageTranslation import PageTranslation
from django.core.cache import cache, get_cache
from lester.utils import parse_page_html, filter_webpage_text, unescape
from lester.CouchDBModels.PageModel import Page_Model
import simplejson as json
import re


class IcePreparation(PageTranslation):

    def __init__(self, project, req, body):
        #self.cache = get_cache('ice')
        super(IcePreparation, self).__init__(project, req, body)


    def sort_text(self, str_type=None):

        page_model = Page_Model(self.project.user, self.project, id=self.original_url, include_strings=False)
        self.project.get_all_translatables(doc_type=['string','form', 'fragment'])
        all_translatables = self.project.make_translatable_models(self.project.all_translatable_docs)

        translatable_dict = dict((mod.raw,mod) for mod in all_translatables)
        #self.cache.set_many(cache_dict)
        self.all_translatable_docs = translatable_dict
        self.page_model = page_model

    def set_models(self):

        soup  = self.translated_soup

        for text_type in self.translatable_types:
            if text_type not in ['image', 'meta']:
                elements  = self.translatables.get(text_type)
                for el in elements:
                    raw = unescape(re.sub( '\s+', ' ', unicode(el)).strip())
                    #print raw
                    if raw in self.all_translatable_docs:
                        model = self.all_translatable_docs.get(raw)
                        replace_node = self.create_replacement(model, raw)
                        el.replaceWith(replace_node)


                    else:
                        self.new_string_list.get(text_type).add(raw)
                self.new_string_list[text_type] = list(self.new_string_list.get(text_type))

        return soup


    def create_replacement(self, model, raw):


        if model.type == 'fragment':
            try:
                translation_tag = model.as_tag(raw=model.translations.get(self.target_lang).get('translation_template'))
            except Exception as e:
                translation_tag = model.as_tag(raw=model.translation_template)

            children = [c for c in translation_tag.descendants]
            for child in children:
                if type(child) == bs4tag and child.get('reverbeo-id',False):

                    try:
                        doc = self.project.elastic_get_by_id(child.get('reverbeo-id'), index_type='translatable')
                        model = self.project.make_translatable_model(doc)
                        replacement = model.as_tag(lang=self.target_lang)
                    except Exception as e:
                        replacement = child

                    replacement['data-string-id'] = model.id
                    replacement['translate-me'] = 'yes'
                    replacement['data-original'] = model.raw
                    replacement.append(" ")
                    child.replace_with(replacement)
        else:
            translation_tag = model.as_tag(lang=self.target_lang)
            translation_tag['data-string-id'] = model.id
            translation_tag['translate-me'] = 'yes'
            translation_tag['data-original'] = model.raw
            translation_tag.append(" ")

        return translation_tag