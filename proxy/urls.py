from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from proxy.views import *

urlpatterns = patterns('',

    url(r'^REVERBEOICE/(.*)$', ice_proxy, name='ice_proxy'),
    url(r'^.*$', proxy_request, name='proxy_request'),
)
