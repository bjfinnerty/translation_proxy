__author__ = 'brianfinnerty'

from bs4 import BeautifulSoup, Tag as bs4tag, Comment as bs4comment, NavigableString as ns4
from urlparse import urlparse
from utils import parse_couch
from ProjectDetails import ProjectDetails
from django.core.cache import cache
from lester.utils import parse_page_html, filter_webpage_text, unescape

import simplejson as json
import couch_connect as couch
import re




class PageTranslation(object):


    """
        Class to deal with all the parsing and modifying of the response body content


    """


    def __init__(self, project, req, body):

        """
            Initialisation of properties that are useful in the methods


            @param - object: RequestPrep()    - request object we created in the first step
            @param - object: ProjectDetails() - the project details we derived from the request
            @param - string: <html></html>    - the body content of the response received after proxying the request through

            Properties:
                project_settings   : object - instance of ProjectDetails()
                request_vars       : object - instance of RequestPrep ()
                target_lang        : string - the language code we are translating the page into eg. 'es', 'fr'
                original_url       : string(url) - the full url of the original page
                new_string_list    : set - empty set for adding new strings to the database
                demo               : Boolean - whether the site is a demo or live site
                page_html          : string <html> or <xml> - the text content of the response body
                plain_soup         : object  - instance of BeautifulSoup(). the repsonse text soupified for modifying
                url_base           : string - the main domain of the page we just requested. used for changing absolute links
                url_base_replace   : string - the domain of the language specific page. used for changing absolute links
                linked_sites       : list - list of the id's of any sites linked to the current one. used for changing absolute links

                text_translations  : method - self.get_project_translations(project.name, self.target_lang, 'string') returns json object of string translations for this project from the database
                form_translations  : method - self.get_project_translations(project.name, self.target_lang, 'form_value') returns json object of form value translations for this project from the database

            Methods:
        """

        body                    = body[body.find("<"):]
        parser                  = getattr(project, 'parse_lib', 'lxml')
        self.new_string_list    = {'string':set(), 'meta':set(), 'form':set(), 'image':set(), 'anchor':set(), 'fragment':set()}
        self.new_string_len     = 0
        self.page_html          = body
        self.plain_soup         = BeautifulSoup(body, parser)
        self.translated_soup    = self.plain_soup

        self.target_lang        = req.lang
        self.original_url       = req.original_url
        self.url_base_replace   = req.host
        self.demo               = req.demo_site
        self.no_cache           = req.no_cache

        self.project            = project
        self.url_base           = project.domain
        self.linked_sites       = project.linked_sites
        self.project_name       = project.name

        self.is_fragment        = self.is_fragment()
        self.translatables      = parse_page_html(self.translated_soup)
        self.translatable_types = ['fragment', 'anchor', 'form', 'meta', 'image', 'string']

        self.sort_text()


    def sort_text(self, str_type=None):
        '''
        @param: str_type: string(optional) - Can be used to specify which tyoe of translation is required.
        calls the get_project_strings function for each type of text
        this function sets the translations as object properties and stores them in the cache to save further db requests
        '''

        all_types = self.translatable_types
        if not str_type:
            types = all_types
        else:
            types = [str_type]

        for text_type in types:
            self.get_project_strings(text_type)

        return


    def get_project_strings(self, text_type):

        '''
        @param: test_type - string ('string'|'meta'|'image'|'form'|'anchor') or anything else we want to translate

        Uses the project name, translatable type, and current language to query the cache to for the translations
        if they are not cached we run different queries for each translatable type
        This is saved as an object property and cached
        '''

        def get_right_translations(translations):
            for string in translations:
                #trans_dict = string
                try:
                    trans_dict = string.get('translations').get(self.target_lang)
                except AttributeError as e:
                    pass
                else:
                    if trans_dict.get('raw_msgstr'):
                        translated[trans_dict.get('raw')] = trans_dict.get('raw_msgstr')
                    else:
                        untranslated.append(trans_dict.get('raw'))


        translated_cache_key = self.project_name + '_' + text_type + '_translated_' + self.target_lang
        untranslated_cache_key = self.project_name + '_' + text_type + '_untranslated_' + self.target_lang

        translated = cache.get(translated_cache_key)
        untranslated = cache.get(untranslated_cache_key)

        if translated == None or untranslated == None or self.no_cache:
            translated = {}
            untranslated = []
            #translations = self.project.view_query(design='translatables', view='translations', extra_params={'key':json.dumps([text_type , self.target_lang])})
            # TODO: Swap this line in when elastic search version is deployed
            translations = self.project.get_raw_translations(self.target_lang)
            get_right_translations(translations)

            cache.set(translated_cache_key, translated)
            cache.set(untranslated_cache_key, untranslated)


        setattr(self, text_type + '_translated_' + self.target_lang, translated)
        setattr(self, text_type + '_untranslated_' + self.target_lang, untranslated)

        return



    def is_fragment(self):
        """
            Checks to se if the response body is a full html page or just a fragment
            returns - Boolean
        """
        if self.page_html.find('<html') == -1 and self.page_html.find('DOCTYPE') == -1 and self.page_html.find('<rss') == -1:
            return True
        else:
            return False



    def unwrap_body(self):
        """
            Gets rid of <html>, <head> and <body> tags from translated response. For use if response body is a fragment
            returns - (translated html) object: instance of BeautifulSoup()
        """

        soup = self.translated_soup
        if soup.html:
            soup.html.unwrap()
        if soup.head:
            soup.head.unwrap()
        if soup.body:
            soup.body.unwrap()
        self.translated_soup = soup
        return self.translated_soup



    def translate_elements(self):

        """
            @param: soup            -    instance of BeautifulSoup(),
            @param: translations    -    dictionary where the key is the string and the value is the translation
            @param: untranslations  -    list of page elements we have scraped but not translated

            Translates the page by:
                using the parsed elements gathered in in the parse_page_html function, stored in the self.translatables property
                loops through these elements using the biggest first (fragments will contain strings that don't need to be retranslated)
                performs a direct string swap based on the raw html element documents in the database
                adds any untranslated strings to a dictionary for posting to the database in a later method

            returns - (translated html) object: instance of BeautifulSoup()
        """
        soup            = self.translated_soup

        for text_type in self.translatable_types:

            elements        = self.translatables.get(text_type)
            translations    = getattr(self, text_type + '_translated_' + self.target_lang, {})
            untranslations  = getattr(self, text_type + '_untranslated_' + self.target_lang, [])


            for el in elements:
                raw = unescape(re.sub( '\s+', ' ', unicode(el)).strip())
                if raw in translations:
                    el.replaceWith(translations.get(raw))
                else:
                    if raw not in untranslations:
                        self.new_string_list.get(text_type).add(raw)
                        self.new_string_len += 1
            self.new_string_list[text_type] = list(self.new_string_list.get(text_type))

        return soup



    def link_to_linked_sites(self, soup, lang):

        """
            @param: soup            -    instance of BeautifulSoup(),
            @param: lang            -    the language code of the target language

            uses the project object to get a list of all sites that are linked to the current one
            this list is a list of the project ids and is used to query the database for their config documents
            the appropriate origin domain and language specific dns is derived to run the mod subs method
            demo is set to false so that only absolute urls are changed in the mod subd function and not relative ones by mistake

            returns - (translated html) object: instance of BeautifulSoup()
        """
        if self.linked_sites:
            for site in self.linked_sites:
                linked_project = couch.get_doc_by_id(site, dbname='all_projects')
                if linked_project:
                    linked = ProjectDetails(linked_project)
                    url_base = urlparse(linked.homepage).netloc
                    url_base_replace = linked.dns.get(lang)
                    soup = self.mod_subs(soup, url_base, url_base_replace, False)
        return soup



    '''def mod_subs(self, soup, url_base, url_base_replace, demo=False):


        """
            @param: soup : obect       -    instance of BeautifulSoup(),
            @param: url_base : string  -    origin domain
            @param: url_base_replace : string  -    new language specific domain
            @param: demo : boolean  -    whether the current page is a demo or not

            iterates through every link on the page <a>, <link>, <src>, <action> and replaces the root of the url with the new language specifc url
            this prevents the user from accidentally clicking off to the origin language domain

            returns - (translated html) object: instance of BeautifulSoup()
        """

        demo = demo or self.demo

        # do the mod subs shit
        all_links = soup.find_all(href=True)
        all_srcs = soup.find_all(src=True)
        all_forms = soup.find_all(action=True)


        if demo:
            for link in all_links:
                if link.get('href').startswith("/") and not link.get('href').startswith('//'):
                    link['href'] = "//" + url_base_replace + link.get('href')
            for src in all_srcs:
                if src.get('src').startswith("/") and not src.get('src').startswith('//'):
                    src['src'] = "//" + url_base_replace + src.get('src')
            for form in all_forms:
                if form.get('action').startswith("/") and not form.get('action').startswith("//"):
                    form['action'] = "//" + url_base_replace + form.get('action')


        for link in all_links:
            link['href'] = link.get('href').replace('http://'+url_base, 'http://'+url_base_replace)
            link['href'] = link.get('href').replace('https://'+url_base, 'https://'+url_base_replace)
            link['href'] = link.get('href').replace('http://www.'+url_base, 'http://'+url_base_replace)
            link['href'] = link.get('href').replace('https://www.'+url_base, 'https://'+url_base_replace)

        for src in all_srcs:
            src['src'] = src.get('src').replace('http://'+url_base, 'http://'+url_base_replace)
            src['src'] = src.get('src').replace('https://'+url_base, 'https://'+url_base_replace)
            src['src'] = src.get('src').replace('http://www.'+url_base, 'http://'+url_base_replace)
            src['src'] = src.get('src').replace('https://www.'+url_base, 'https://'+url_base_replace)

        for form in all_forms:
            form['action'] = form.get('action').replace('http://'+url_base, 'http://'+url_base_replace)
            form['action'] = form.get('action').replace('https://'+url_base, 'https://'+url_base_replace)
            form['action'] = form.get('action').replace('http://www.'+url_base, 'http://'+url_base_replace)
            form['action'] = form.get('action').replace('https://www.'+url_base, 'https://'+url_base_replace)


        self.translated_soup = soup
        return self.translated_soup'''
