__author__ = 'brianfinnerty'

import re
import simplejson as json

from django.core.cache import cache
from utils import findnth, is_demo
from babel import Locale
from urlparse import urlparse, urlunparse
from lester.CouchDBModels.ProjectModel import Project
from lester.CouchDBModels.UserModels import *
from django.http import Http404

class RequestPrep(object):


    """
        Class to deal with all the parsing and modifications of requests after they are received from the WSGI app and before they are forwarded to the original url
        @param - request object
    """

    def __init__(self, r):

        """
            intializes the class and sets the necessary attributes
            @param - request object
            returns - nothing
        """
        self.no_cache        = False
        r                    = self._cache(r)
        self.request_uri     = r.build_absolute_uri()
        self.path            = r.get_full_path()
        self.scheme          = r.META.get('wsgi.url_scheme')
        self.host            = r.get_host()
        self.wsgi_headers    = r.META
        self.method          = r.method
        self.post_data       = None
        self.demo_site       = is_demo(self.request_uri)
        self.files           = r.FILES
        self.lang            = self.host[:2]
        self.cookies         = r.COOKIES
        self.user_agent      = r.META.get('HTTP_USER_AGENT')

        if self.method == "POST":
            self.set_post_data(r.POST)


    def _cache(self, r):

        if 'no_cache' in r.GET:
            cache_bool = r.GET.get('no_cache')
            qs = 'no_cache=' + cache_bool
            r.META['QUERY_STRING'] = r.META.get('QUERY_STRING').replace(qs, '')
            if cache_bool in ['true', 'yes', '1', '']:
                self.no_cache = True

        return r


    def get_project(self, user):

        if cache.get(self.host):
            return cache.get(self.host)
        else:
            try:
                project_doc = user.view_query(design='filter', view='by_dns', db_name='all_projects/', extra_params={'key':json.dumps(self.host)})[0]
            except IndexError as e:
                raise Http404()
            else:
                project = Project(user, id=project_doc.get('name'), include_summary=False)
                cache.set(self.host, project)
                return project



    def set_post_data(self, post):

        """
            if there is data posted to the server, save it in the post_data attribute
            intializes the class and sets the necessary attributes
            @param - post data QueryDict()
            returns - self
        """

        self.post_data = post
        return self




    def parse_url_frags(self):

        """
            Function to parse any reverbeo specific urls that come through our host server.
            Urls should be one of the following forms
                - {lc}.{domain.com}/{path}?
                - {lc_LC}.{domain.com}/{path}?
                - demo{2}?.reverbeo.com/{lc}/{domain.com}/{path}?
                - demo{2}?.reverbeo.com/{lc_LC}/{domain.com}/{path}?

                @param - req_vars.req_url    - the full request url         eg. http://fr.reverbeo.com/blog/ , http://demo2.reverbeo.com/pt_PT/getsoundwave.com/
                @param - req_vars.scheme     - the proper request scheme    eg. http or https
                @param - req_vars.demo_site  - Boolean                      eg. Boolean indicating whether site is live or a demo

                returns - GlobalVars.Request
        """

        req_url = self.request_uri
        scheme  = self.scheme
        demo    = self.demo_site

        valid_parts = req_url.replace(scheme+'://','')

        if demo:

            # valid_parts is in the form fr-getsoundwave.reverbeo.com/path.....

            #lang_frag = valid_parts[:findnth(valid_parts,"/",1)][findnth(valid_parts,"/",0):].strip("/")
            sub_dom = valid_parts[:findnth(valid_parts,".", 0)]
            lang_frag = sub_dom[:findnth(sub_dom, "-", 0)]

        else:
            # valid_parts is in the fr.getsoundwave.com/path.....

            lang_frag = valid_parts[:findnth(valid_parts,".",0)]
        dns = valid_parts[:findnth(valid_parts,"/",0)]
        path = valid_parts[findnth(valid_parts,"/",0):]

        self.set_proxy_params(lang_frag, path, dns)

        return self



    def set_proxy_params(self, lang, path, domain):
        """
            setting attributes obtained from the parse_url_frags method

            @param - lang    - string: {a-z}2 language parameter             eg. 'es', 'fr'
            @param - path    - string: the actual path of the original url   eg. "/", "/contact.html", "/dynamic/?query=string"
            @param - domain  - string: the domain of the request url         eg. "es.wildrovertours.com"
        """
        self.remote_path = path
        self.lang = Locale.parse(lang).language
        self.dns = domain
        return self



    def resolve_original_url(self, project_homepage):
        homepage_netloc = urlparse(project_homepage).netloc
        self.original_url = urlunparse([self.scheme, homepage_netloc, self.path, "","",""])
        return self.original_url


    def set_http_request_headers(self):

        headers = self.wsgi_headers
        origin_url = self.original_url
        regex = re.compile('^HTTP_')

        http_headers = dict((regex.sub('', header).replace('_','-'), value) for (header, value)
            in headers.items() if header.startswith('HTTP_') and header != "HTTP_COOKIE")

        http_headers['HOST'] = urlparse(origin_url).netloc

        if http_headers.get('REFERER'):
            http_headers['REFERER'] = origin_url

        if http_headers.get('ORIGIN'):
            http_headers['ORIGIN'] = origin_url

        if http_headers.get('CONTENT-TYPE') and http_headers.get('CONTENT-TYPE').startswith('multipart/form-data'):
            del http_headers['CONTENT-TYPE']
            del http_headers['CONTENT-LENGTH']

            if self.files:
                files = {}
                for input, val in self.files.items():
                    files[input] = (val.name, val.read())
            else:
                files = {
                    '':""
                }
        else:
            files = None
        self.files = files
        self.http_req_headers = http_headers

        return http_headers


