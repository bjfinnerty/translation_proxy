from django.http import HttpResponse, Http404
from utils import *

class ResponseWrapper(object):

    def __init__(self, resp, project, req):

        self.original_content   = resp.content
        self.status             = resp.status_code
        self.cookies            = resp.cookies
        self.headers            = resp.headers
        self.actual_url         = resp.url

        # this is the translated content before it is translated
        self.translated_content = self.original_content

        self.url_base           = project.domain
        self.url_base_replace   = req.host

        # create blank response object for returning
        self.translated_response = HttpResponse(status=self.status, content_type=self.headers.get('content-type'))



    def modify_response_cookies(self):

        # modify domain and path of any cookies returned from the server to preserve persistence

        cookies = self.cookies

        for cookie in cookies:
            new_cookie_dom = self.url_base_replace
            new_cookie_path = cookie.path
            cookie.domain = new_cookie_dom
            cookie.path = new_cookie_path

        return cookies



    def modify_response_headers(self, content):

        # WSGI or Django can't return a response with these headers. Investigate further if it becomes a problem
        # manually delete 'set-cookie' header and deal with it in response.cookies object
        # Reset the content length to reflect the modified response

        headers = self.headers

        disallowed_headers = ['connection',
                              'keep-alive',
                              'proxy-authenticate',
                              'proxy-authorization',
                              'te',
                              'trailers',
                              'transfer-encoding',
                              'upgrade',
                              'set-cookie',
                              'content-encoding'
        ]
        if self.cookies:
            disallowed_headers.append('set-cookie')

        for hdr in headers:
            if hdr in disallowed_headers:
                del headers[hdr]
            else:
                if type(headers[hdr]) == str and self.url_base in headers[hdr]:
                    regex_url_base = re.compile('(www.)?%s'%self.url_base)
                    headers[hdr] = regex_url_base.sub(self.url_base_replace, headers[hdr])

        headers['content-length'] = str(len(content))

        return headers


    def set_translated_cookies(self):
        self.translated_cookies = self.modify_response_cookies()
        for cookie in self.translated_cookies:
            try:
                self.translated_response.set_cookie(cookie.name, value=cookie.value, path=cookie.path, domain=cookie.domain, secure=cookie.secure, expires=cookie.expires)
            except AttributeError, e:
                print e, cookie.name
        return self.translated_response



    def set_translated_headers(self):
        self.translated_headers = self.modify_response_headers(self.translated_content)
        for key, val in self.translated_headers.items():
            self.translated_response[key] = val
        return self.translated_response

