__author__ = 'brianfinnerty'

from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache

import requests, logging


from utils import *
from ProxyRequest import RequestPrep
from PageTranslation import PageTranslation
from ResponseWrapper import ResponseWrapper

from settings import PROXY_DB_USER, PROXY_DB_PASS
logger = logging.getLogger(__name__)


from celerytasks.tasks import queue_strings
from lester.CouchDBModels.UserModels import User

from Cookie import SimpleCookie
SimpleCookie.value_encode = SimpleCookie.value_decode
from django.http import HttpResponse
from mixpanel import Mixpanel


@csrf_exempt
def proxy_request(request):


    """
        This is the one function this app has. it takes in a wsgi request and returns a http response
        Requests are filtered so as not to be for static assets like images, javascript, css etc. We should only deal with requests for texl/html, and text/xml mimetypes (Support for json should be further down the line)

        @param: wsgi request passed through nginx via uwsgi
                - the most important part of the request is the URL. It should contain a language parameter, a domain which we will use to query the database for the translations
                - the path will be used to proxy the request through to the original site to get the origin html

        The request uri is broken down to its most pertinent parts - a language parameter and a domain
        The domain is the key used to query the database for the right settings file and translations
        The request headers are changed from the language specific domain to the original domain eg. Host:fr.reverbeo.com -> Host:www.reverbeo.com
        The original url (of the corresponding page on the origin site, without the language parameter) is reconstructed and a request is sent through
        If the response doesnt return 200, the response is the response content is altered by:
            - the display text, meta data and form values are translated based on the values stored in the database
            - the links are changed to absolute so they resolve properly
            - any untranslated strings are added to the database
        Any cookies returned from the server are modified from the original domain to the language specific domain
        Any headers are modified likewise. Some headers disallowed by django are deleted

        Returns: http response with correct headers, status and body content
    """

    user = User(username=PROXY_DB_USER, password=PROXY_DB_PASS)
    req = RequestPrep(request)
    project = req.get_project(user)

    # get the original url from the project details and the request details
    req.original_url = req.resolve_original_url(project.homepage)

    # modify the request headers to make sure the request is successful
    http_req_headers = req.set_http_request_headers()

    # ===== THIS IS WHERE WE ARE FIRING THE REQUEST ===== #
    try:
        resp = requests.request(req.method, req.original_url, allow_redirects=False, headers=http_req_headers, data=req.post_data, files=req.files, cookies=req.cookies)
    except Exception as e:
        logger.exception(e)
        if cache.get(req.request_uri[:240]):
            return HttpResponse(status=200, content=cache.get(req.request_uri[:240]))
        else:
            return HttpResponse(status=503, content='')
    else:
        proxy_response = ResponseWrapper(resp, project, req)

        if proxy_response.status == 200:

            if ('text/html' or 'application/rss+xml') in resp.headers.get('content-type'):

                # Look for this response in the cache and compare. If it is the same skip the translation steps and just write the same content
                cache_content = cache_vars(req, proxy_response.original_content)

                if cache_content:
                    proxy_response.translated_content = cache_content
                else:
                    cache.set(req.original_url[:240], proxy_response.original_content, 60*5)

                    # initialize page object
                    page = PageTranslation(project, req, proxy_response.original_content)

                    soup = page.plain_soup

                    # translate all page elements
                    soup = page.translate_elements()

                    # if the original response did not contain html tags, remove them from the translated response
                    if page.is_fragment:
                        soup = page.unwrap_body()

                    # send data to mixpanel to track who is accessing the proxy
                    mp = Mixpanel("26c0bd3eb0bb00a1f8a608404cb61fb5")
                    mp.track(req.request_uri, event_name=req.method + ' ' + req.request_uri, properties={'new_strings':page.new_string_len,
                                                                                                         'user_agent':req.user_agent})

                    # add that translated content to the response object
                    proxy_response.translated_content = str(page.translated_soup.encode(formatter=None))

                    # add any new strings to the database and the page if the url did not exist previously
                    if page.new_string_len:
                        try:
                            queue_strings.delay(user, project, page.original_url, page.new_string_list, page.new_string_len)
                        except Exception as e:
                            logger.exception(e)
                    else:
                        # cache the translated content so we dont have to run process again if there are no new strings
                        cache.set(req.request_uri[:240], proxy_response.translated_content, 60*20)
            else:
                cache.set(req.request_uri[:240], proxy_response.translated_content, 60*60)
                proxy_response.translated_content = proxy_response.original_content

        proxy_response.set_translated_cookies()
        proxy_response.set_translated_headers()
        proxy_response.translated_response.content = proxy_response.translated_content

        return proxy_response.translated_response





def cache_vars(req, origin_cont):

    if req.no_cache:
        cache.delete(req.request_uri[:240])
        cache.delete(req.original_url[:240])
        return False
    else:
        origin_html_cached = cache.get(req.original_url[:240])
        translated_html_cached = cache.get(req.request_uri[:240])

        if origin_cont == origin_html_cached and translated_html_cached:
            return translated_html_cached
        else:
            return False
