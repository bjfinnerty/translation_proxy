from django.test import TestCase
import couch_connect as couch
from utils import parse_couch, parse_couch_keys_unique, findnth
from urlparse import urlsplit, urljoin, urlparse, urlunparse
from babel import Locale
import requests
import warnings
import re
import simplejson as json
from language_codes import language_codes

from django.test.client import FakePayload
from views import *
from ProxyRequest import RequestPrep
from PageTranslation import PageTranslation
from ResponseWrapper import ResponseWrapper



test_docs = parse_couch(couch.view_query('all_docs', 'all_objs', dbname='unit_test_data'))
first_test_url = "http://bubbles.host.reverbeo.com/pl_PL/tempity.com/GET"



wsgi_params = {'uwsgi.node': 'ip-10-34-202-137',
               'uwsgi.version': '1.9.11',
               'wsgi.multiprocess': True,
               'wsgi.multithread': False,
               'wsgi.run_once': False,
               'wsgi.version': (1, 0),
               'wsgi.input':FakePayload('')}


class GeneralTests(TestCase):

    def test_is_demo(self):

        demo_sites = ['es-sightergame.reverbeo.com/osomepath?page=1',
                      'en-sportid.reverbeo.com',
                      'ar-httpwwwlocalsapparelse.reverbeo.com',
                      'da-lime-canvas.reverbeo.com']

        live_sites = ['es.booking.wildrovertours.com',
                      'it.globalcview.com',
                      'ar.tixbox.com',
                      'es.reverbeo.com',
                      'www.reverbeo.it']

        for rec in demo_sites:
            demo_function_result = is_demo("http://"+rec)
            print 'demo', demo_function_result, rec
            self.assertTrue(demo_function_result)

        for rec in live_sites:
            demo_function_result = is_demo("http://"+rec)
            print 'demo', demo_function_result, rec
            self.assertFalse(demo_function_result)




    def test_all_my_locales(self):

        for code, arr in language_codes.items():
            print code, arr[0]
            self.assertTrue(Locale.parse(code))
            self.assertTrue(Locale.parse(arr[0]))


"""
class ProcessRequestTestCase(TestCase):


    def test_request_vars(self):

        for doc in test_docs:
            r = create_request(doc.get('_id'))
            test_results = couch.get_doc_by_id(doc.get('_id'), dbname="unit_test_data")
            req = RequestPrep(r)

            self.assertEqual(req.request_uri, test_results.get('request_object').get('request_uri'))
            self.assertEqual(req.path, test_results.get('request_object').get('path'))
            self.assertEqual(req.scheme, test_results.get('request_object').get('scheme'))
            self.assertEqual(req.host, test_results.get('domain'))
            #self.assertEqual(json.dumps(req.wsgi_headers), json.dumps(test_results.get('wsgi_headers')))
            self.assertEqual(req.method, test_results.get('method'))




    def test_parse_url_frags(self):

        for doc in test_docs:
            r = create_request(doc.get('_id'))
            test_results = couch.get_doc_by_id(doc.get('_id'), dbname="unit_test_data")
            req = RequestPrep(r)
            req.parse_url_frags()

            self.assertEqual(req.lang, test_results.get('request_object').get('lang'))
            self.assertEqual(req.remote_path, test_results.get('request_object').get('remote_path'))
            self.assertEqual(req.dns, test_results.get('request_object').get('dns'))



    def test_modified_headers(self):

        for doc in test_docs:
            r = create_request(doc.get('_id'))
            test_results = couch.get_doc_by_id(doc.get('_id'), dbname="unit_test_data")
            if not test_results.get('project_object'):
                print "ERROR HERE, ",  doc.get('_id')
            project_doc = couch.get_doc_by_id(test_results.get('project_object').get('name'), dbname='all_projects')
            if project_doc:
                project_homepage = project_doc.get('homepage')

                req = RequestPrep(r)
                req = req.parse_url_frags()
                req.original_url = req.resolve_original_url(project_homepage)
                modified_headers = req.set_http_request_headers()

                if modified_headers.get('HOST'):
                    self.assertEqual(urlparse(req.original_url).netloc, modified_headers.get('HOST'))

                if modified_headers.get('ORIGIN'):
                    self.assertEqual(urlparse(req.original_url).netloc, modified_headers.get('ORIGIN'))


class ResponseModifications(TestCase):

    def test_cookie_mod(self):

        disallowed_headers = ['connection',
                              'keep-alive',
                              'proxy-authenticate',
                              'proxy-authorization',
                              'te',
                              'trailers',
                              'transfer-encoding',
                              'upgrade',
                              'set-cookie',
                              'content-encoding'
        ]

        for doc in test_docs:
            test_results = couch.get_doc_by_id(doc.get("_id"), dbname="unit_test_data")
            req_obj = test_results.get('request_object')
            proj_obj = test_results.get('project_object')
            page_obj = test_results.get('page_object')
            resp_obj = test_results.get('response_object')
            resp_obj['cookies'] = requests.utils.cookiejar_from_dict(resp_obj.get('cookies'))

            proxy_resp = ResponseWrapper(resp_obj, proj_obj, req_obj)

            new_cookies = proxy_resp.modify_response_cookies()
            for cookie in new_cookies:
                self.assertEqual(cookie.domain + cookie.path.rstrip("/"),req_obj.get('dns'))

            new_headers = proxy_resp.modify_response_headers(resp_obj.get('translated_content').encode('utf-8'))
            self.assertEqual(new_headers, resp_obj.get('translated_headers'))
            #self.assertAlmostEquals(new_headers, resp_obj.get('translated_headers'))





class TranslationProcessTestCase(TestCase):


                page = PageTranslation(project, req, response_content)
                soup = page.mod_subs(page.plain_soup, page.url_base, page.url_base_replace)
                if page.linked_sites:
                    soup = page.link_to_linked_sites(soup, page.lang)
                soup = page.translate_display_text(soup, page.text_translations)
                soup = page.translate_form_values(soup, page.form_translations)
                if page.is_fragment:
                    soup = page.unwrap_body()


    def test_initalize(self):

        for doc in test_docs:

            print doc.get('url')

            test_results = couch.get_doc_by_id(doc.get("_id"), dbname="unit_test_data")
            req_obj = test_results.get('request_object')
            proj_obj = test_results.get('project_object')
            page_obj = test_results.get('page_object')
            resp_obj = test_results.get('response_object')

            #print json.loads(resp_obj.get('original_content'))

            page = PageTranslation(proj_obj, req_obj, resp_obj.get('original_content'))

            #self.assertEqual(type(page.text_translations), dict())
            #self.assertEqual(type(page.form_translations), dict())
            self.assertEqual(page.is_fragment, page_obj.get('is_fragment'))

            soup = page.plain_soup
            # all the functions below should take a Beautiful Soup object as a parameter and return a Beautiful Soup object.
            # because the page content and the translations are dynamic we cant test for anything more than that

            new_soup2 = page.translate_display_text(soup, page.text_translations)
            new_soup3 = page.translate_form_values(soup, page.form_translations)
            new_soup4 = page.bull_mod_subs(page.page_html, page.url_base, page.url_base_replace, page.demo)
            new_soup1 = page.unwrap_body()
            new_soup5 = page.link_to_linked_sites(soup, page.target_lang)
            self.assertTrue(isinstance(new_soup1, BeautifulSoup))
            self.assertTrue(isinstance(new_soup2, BeautifulSoup))
            self.assertTrue(isinstance(new_soup3, BeautifulSoup))
            self.assertTrue(isinstance(new_soup4, BeautifulSoup))
            self.assertTrue(isinstance(new_soup5, BeautifulSoup))



"""


