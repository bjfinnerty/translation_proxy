

class ProjectDetails(object):

    def __init__(self, project_doc):

        self.project_doc    = project_doc
        self.name           = project_doc.get('name')
        self.homepage       = project_doc.get('homepage')
        self.domain         = project_doc.get('domain')
        self.linked_sites   = project_doc.get('linked_sites')
        self.owner          = project_doc.get('owner')
        self.target_langs   = project_doc.get('target_langs')
        self.source_lang    = project_doc.get('source_lang')
        self.mt             = project_doc.get('machine_translate')
        self.dns            = project_doc.get('dns')








